-- Translation by: Trilip
-- Correction by: Oulan
zMailModData.LANGUAGES[SystemData.Settings.Language.FRENCH] =
{
    TITLE_LOG             = L"Historique zMailMod",
    TITLE_OPTIONS         = L"Options zMailMod",

    INIT_MESSAGE          = L"zMailMod charg�.",

    LABEL_POSTAGE         = L"Frais:",
    LABEL_TIME_TO_WAIT    = L"Temps d'attente:",
    LABEL_MONEY           = L"Somme envoy�e:",

    BUTTON_SELECT_MONEY   = L"S�lectionner l'argent",
    BUTTON_SELECT_ALL     = L"Tout s�lectionner",
    BUTTON_OPEN_SELECTED  = L"Collecter la s�lection",

    MESSAGE               = L"message",
    MESSAGES              = L"messages",

    TIME_MINUTE           = L"min",
    TIME_MINUTES          = L"min",
    TIME_HOUR             = L"h",
    TIME_HOURS            = L"h",
    TIME_DAY              = L"jour",
    TIME_DAYS             = L"jours",

    GOLD                  = L"o",
    SILVER                = L"a",
    BRASS                 = L"b",
    
    AND                   = L"et",
    BLACK                 = L"noir",

    GENERAL               = L"G�n�ral",
    QUEUE                 = L"File d'attente",
    LAYOUT                = L"Mise en page",
    AUTOCOMPLETE          = L"Compl�ter automatiquement",
    AUTOCOMPLETE_ENABLE   = L"Nom de l'article � l'objet",
    BACKPACK              = L"Sac � dos",
    BACKPACK_OPEN         = L"Ouvrir le sac avec la fen�tre Mail",
    BACKPACK_CLOSE        = L"Fermer le sac avec la fen�tre Mail",
    EXPIRE_TIME           = L"D�lai d'expiration",
    EXPIRE_WARNING        = L"Seuil d'avertissement (en jours)",
    LAST_RECIPIENT        = L"Dernier destinataire",
    LAST_RECIPIENT_SAVE   = L"Se souvenir du dernier destinataire",
    INPUT_COMBINATION     = L"Combinaison de touches",
    INPUT_KEY             = L"Touche du clavier",
    KEY_CTRL              = L"Ctrl",
    KEY_ALT               = L"Alt",
    KEY_SHIFT             = L"MAj",
    NONE                  = L"Aucun",
    INPUT_MOUSE           = L"Bouton de souris",
    LEFT                  = L"Gauche",
    RIGHT                 = L"Droite",
    QUEUE_TIMERS          = L"D�lai",
    QUEUE_COOLDOWN        = L"Temps de recharge entre envoi",
    QUEUE_RETRIES         = L"Nombre de tentatives",
    QUEUE_DELAY           = L"D�lai avant de recommencer",
    DEFAULT               = L"Par d�faut",
    MASSMAIL              = L"MassMail",
    MASSADD_ENABLE        = L"Afficher les boutons suppl�mentaires",

    LOG_HEADER_SENDER     = L"Exp�diteur",
    LOG_HEADER_TYPE       = L"Type",
    LOG_HEADER_MONEY      = L"Argent",
    LOG_HEADER_ITEM       = L"Article",
    
    TOOLTIP_MONEY         = L"Ce message contient :",

    TRY_LATER             = L"R�essayez ult�rieurement.",
    RETRY                 = L"Essayez � nouveau dans  <<1>> sec",

    AUCTION_COMPLETE      = L"Vente aux encheres termin�e!",

    TEXT_MAIL_RESULT87    = L"Le serveur de messagerie ne r�pond pas.",
    TEXT_MAIL_RESULT88    = L"Votre sac � dos est plein.",
    TEXT_MAIL_RESULT89    = L"Collecte termin�e.",
    TEXT_MAIL_RESULT90    = L"Vous devez entrer le destinataire.",
    TEXT_MAIL_RESULT91    = L"Vous ne pouvez pas envoyer un mail � vous-m�me.",
    TEXT_MAIL_RESULT92    = L"Vous ne pouvez pas envoyer tous les mails.",
    TEXT_MAIL_RESULT93    = L"Collecte... 1 message retir�.",
    TEXT_MAIL_RESULT94    = L"Collecte... <<2>> messages retir�s.",
    TEXT_MAIL_RESULT95    = L"�chec de la collecte.",
    TEXT_MAIL_RESULT96    = L"Envoi �chou�.",
    TEXT_MAIL_RESULT97    = L"<<1>> des <<2>> articles envoy�s. Attente de rechargement: <<3>> sec",
    TEXT_MAIL_RESULT98    = L"1 article envoy� r�ussi.",
    TEXT_MAIL_RESULT99    = L"<<2>> articles envoy�s r�ussi.",
}
