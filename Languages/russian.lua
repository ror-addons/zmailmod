-- Translation by Dmitry Shatalov
zMailModData.LANGUAGES[SystemData.Settings.Language.RUSSIAN] =
{
    TITLE_LOG             = L"zMailMod Лог",
    TITLE_OPTIONS         = L"zMailMod Настройки",

    INIT_MESSAGE          = L"zMailMod загружен.",

    LABEL_POSTAGE         = L"Оплата:",
    LABEL_TIME_TO_WAIT    = L"Время ожидания:",
    LABEL_MONEY           = L"Сумма для отправки:",

    BUTTON_SELECT_MONEY   = L"Выбрать деньги",
    BUTTON_SELECT_ALL     = L"Выбрать все",
    BUTTON_OPEN_SELECTED  = L"Открыть выбр.",

    MESSAGE               = L"сообщение",
    MESSAGES              = L"сообщения",

    TIME_MINUTE           = L"мин",
    TIME_MINUTES          = L"мин",
    TIME_HOUR             = L"ч",
    TIME_HOURS            = L"ч",
    TIME_DAY              = L"день",
    TIME_DAYS             = L"дней",

    GOLD                  = L"g",
    SILVER                = L"s",
    BRASS                 = L"b",

    AND                   = L"и",
    BLACK                 = L"прозр.",

    GENERAL               = L"Главные",
    QUEUE                 = L"Клав./тайм.",
    LAYOUT                = L"Показ",
    AUTOCOMPLETE          = L"Автоокончание",
    AUTOCOMPLETE_ENABLE   = L"Вкл. автоокончание имен",
    BACKPACK              = L"Рюкзак",
    BACKPACK_OPEN         = L"Откр. рюкзак вместе с окном почты",
    BACKPACK_CLOSE        = L"Закр. рюкзак вместе с окном почты",
    EXPIRE_TIME           = L"Срок храниния письма",
    EXPIRE_WARNING        = L"Срок действия (в днях)",
    LAST_RECIPIENT        = L"Последний адресат",
    LAST_RECIPIENT_SAVE   = L"Запомнить последнего адресата",
    INPUT_COMBINATION     = L"Комбинация клавиш ввода",
    INPUT_KEY             = L"Клавиша ввода",
    KEY_CTRL              = L"Ctrl",
    KEY_ALT               = L"Alt",
    KEY_SHIFT             = L"Shift",
    NONE                  = L"Ничего",      
    INPUT_MOUSE           = L"Клавиша мыши",
    LEFT                  = L"Левая",
    RIGHT                 = L"Правая",
    QUEUE_TIMERS          = L"Таймеры",
    QUEUE_COOLDOWN        = L"Доп. время ожидания между отпр.",
    QUEUE_RETRIES         = L"Число повторов",
    QUEUE_DELAY           = L"Время между повторами",
    DEFAULT               = L"По умолч.",
    MASSMAIL              = L"Групповая отправка",
    MASSADD_ENABLE        = L"Показывать кнопки групп. доб.",

    LOG_HEADER_SENDER     = L"Отправитель",
    LOG_HEADER_TYPE       = L"Тип",
    LOG_HEADER_MONEY      = L"Деньги",
    LOG_HEADER_ITEM       = L"Вещь",

    TOOLTIP_MONEY         = L"Это сообщение содержит:",

    TRY_LATER             = L"Попробуйте снова позже.",
    RETRY                 = L"Пробуем снова через <<1>> сек",

    AUCTION_COMPLETE      = L"Аукцион окончен!",                     

    TEXT_MAIL_RESULT87    = L"Почтовый сервер не отвечает.",
    TEXT_MAIL_RESULT88    = L"Рюкзак полон.",
    TEXT_MAIL_RESULT89    = L"Сбор завершен.",
    TEXT_MAIL_RESULT90    = L"Необходимо ввести адресата.",
    TEXT_MAIL_RESULT91    = L"Невозможно отправить почту самому себе.",
    TEXT_MAIL_RESULT92    = L"Невозможно отправить все письма.",
    TEXT_MAIL_RESULT93    = L"Сбор... 1 письмо обработано.",
    TEXT_MAIL_RESULT94    = L"Сбор... <<2>> сообщения(ий) обработано.",
    TEXT_MAIL_RESULT95    = L"Сбор прошел неудачно.", 
    TEXT_MAIL_RESULT96    = L"Отправка неудалась.", 
    TEXT_MAIL_RESULT97    = L"<<1>> из <<2>> вещей отправлено. До окончания паузы: <<3>> сек",
    TEXT_MAIL_RESULT98    = L"1 вещь отправлена успешно.",
    TEXT_MAIL_RESULT99    = L"<<2>> вещей отправлено успешно.",
}