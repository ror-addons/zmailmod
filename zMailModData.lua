--[[
    --> file: zMailModData.lua

    --> description: contains values of global variables, this file has to be loaded first
    --> note: except for few variables, this file shouldn't be edited

    --> author: zoog
    --> email: ZoogTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

zMailModData  =
{
    NAME                 = "zMailMod",                     -- addon's name
    VERSION              = "0.9.9.5",                      -- version number
    
    PLAYER_NAME          = "",                             -- holder for the player's name, is set during initialization

    WINDOW_INBOX         = "MailWindowTabInbox",           -- Inbox window
    WINDOW_SEND          = "MailWindowTabSend",            -- Send window
    WINDOW_AUCTION       = "MailWindowTabAuction",         -- Auction window
    WINDOW_MESSAGE       = "MailWindowTabMessage",         -- Message window
    WINDOW_MASSMAIL      = "zMailModMassMail",             -- MassMail window
    WINDOW_OPTIONS       = "zMailModOptions",              -- Options window
    WINDOW_LOG           = "zMailModLog",                  -- Log window
    
    TAB_MASSMAIL         = "zMailModTabMassMail",          -- MassMail tab

    CD_SHORT             = 1,                              -- cooldown for alts and guildmates
    CD_LONG              = 5,                              -- cooldown for others
    COOLDOWN             = 5,                              -- cooldown, default value set to long

    DEFAULT_SLOTS        = 28,                             -- default number of buttons visible at the same time
    DEFAULT_ROWS         = 4,                              -- default number of rows of buttons
    SLOTS_IN_ROW         = 7,                              -- default number of buttons in one row

    ITEM_SLOTS           = 28,                             -- number of slots, can be automatically increased if needed
    NUM_ROWS             = 4,                              -- number of rows, can be automatically increased if needed
    OFFSET               = 0,                              -- scroll position

    EMPTY_SLOT           = 20,                             -- number of icon for empty slot

    DEFAULT              =                                 -- default values for saved variables
    {
        AUTOCOMPLETE     = 1,                              -- names autocomplete (0 = no, 1 = yes)
        BAGS_OPEN        = 1,                              -- open bags with mail window (0 = no, 1 = yes)
        BAGS_CLOSE       = 1,                              -- close bags with mail window (0 = no, 1 = yes)
        EXPIRE_WARNING   = 1,                              -- time below which expire time will be displayed in red color (in days)
        KEY              = 8,                              -- keyboard key (8 = Ctrl, 32 = Alt, 4 = Shift, 0 = none)
        MOUSE            = 2,                              -- mouse button (1 = Left, 2 = Right)
        ERRORS           = 5,                              -- number of errors after which zMailMod should stop trying to send mail
        PAUSE_TIME       = 5,                              -- retry delay time (in seconds)
        EXTRA_COOLDOWN   = 2,                              -- extra delay time before sending a mail (min = 1, recommended is 2) (in seconds)
        LAYOUT_INBOX     = 1,                              -- Inbox and Auction tabs layout (0 = default, 1 = zMailMod, 2 = zMailMod black)
        LAYOUT_SEND      = 1,                              -- Send tab layout (0 = default, 1 = zMailMod)
        LAYOUT_MESSAGE   = 1,                              -- Message window layout (0 = default, 1 = zMailMod)
        MASSADD_ENABLE   = 0,                              -- show MassAdd buttons (0 = no, 1 = yes)
        LAST_RECIPIENT   = 0,                              -- save last recipient (0 = no, 1 = yes)
    },
    
    LAYOUTS              =                                 -- configuration of inbox/auction layouts #TODO: bad idea, remove this
    {
        [0]              =
        {
            font         = "font_default_text_small",
            timeAlign    = "leftcenter",
            timeColor    = {r = 0,   g = 200, b =   0},
            timeWarning  = {r = 200, g = 0,   b =   0},
            fromUnread   = {r = 255, g = 255, b = 255},
            fromRead     = {r = 255, g = 255, b = 255},
            subjUnread   = {r = 255, g = 255, b = 255},
            subjRead     = {r = 255, g = 255, b = 255},
            itemUnread   = {r = 255, g = 255, b = 255, a = 1},
            itemRead     = {r = 255, g = 255, b = 255, a = 1},
        },
        
        [1]              =
        {
            font         = "font_chat_text_no_outline",
            timeAlign    = "rightcenter",
            timeColor    = {r =   0, g = 100, b =   0},
            timeWarning  = {r = 175, g =   0, b =   0},
            fromUnread   = {r =  44, g =  33, b =  20},
            fromRead     = {r =  75, g =  75, b =  75},
            subjUnread   = {r =  82, g =  45, b =  20},
            subjRead     = {r =  75, g =  75, b =  75},
            itemUnread   = {r = 255, g = 255, b = 255, a = 1},
            itemRead     = {r = 100, g = 100, b = 100, a = 0.8},
        },
        
        [2]              =
        {
            font         = "font_chat_text_no_outline",
            timeAlign    = "rightcenter",
            timeColor    = {r = 0,   g = 200, b =   0},
            timeWarning  = {r = 200, g = 0,   b =   0},
            fromUnread   = {r = 255, g = 204, b = 102},
            fromRead     = {r = 100, g = 100, b = 100},
            subjUnread   = {r = 255, g = 255, b = 255},
            subjRead     = {r = 125, g = 125, b = 125},
            itemUnread   = {r = 255, g = 255, b = 255, a = 1},
            itemRead     = {r = 100, g = 100, b = 100, a = 0.8},
        },
    },

    LANGUAGES            = {},                             -- table for localized strings
    LANGUAGE             = SystemData.Settings.Language.active,   -- current language in game

    ITEMS_TO_SEND        = 0,                              -- number of items to send
    ITEMS_SENT           = 0,                              -- number of items that has been sent

    TIME_TO_WAIT         = 0,                              -- time to wait before next mail can be sent
    TOTAL_TIME           = 0,                              -- time needed to send all mails

    IDLE                 = 1,                              -- state: not sending
    SENDING              = 2,                              -- state: sending
    OPENING              = 3,                              -- state: retrieving
    READY                = 4,                              -- state: sending and ready to send next mail
    WAITING              = 5,                              -- state: sending and on cooldown
    PAUSED               = 6,                              -- state: sending and waiting for an answer from the mail server
    INBOX                = 7,                              -- state: opening in inbox
    AUCTION              = 8,                              -- state: opening in auction

    JOB                  = 1,                              -- default state: not sending
    STATE                = 4,                              -- default state: ready to send next mail

    CURRENT_ITEMS        = {},                             -- data of items that are currently being sent

    UNREAD_INBOX         = 0,                              -- number of unread mails in Inbox tab
    UNREAD_AUCTION       = 0,                              -- number of unread mails in Auction tab

    ITEMS_MAX            = 16,                             -- number of items in single message
    QUEUE                = {},                             -- queue of items to send
    HOOKED               = {},                             -- table for hooked functions

    KEYBOARD_DEVICE_ID   = 1,                              -- id for keyboard

    KEY_UP_ARROW         = 200,                            -- id for up arrow
    KEY_DOWN_ARROW       = 208,                            -- id for down arrow
    KEY_LEFT_ARROW       = 203,                            -- id for left arrow
    KEY_RIGHT_ARROW      = 205,                            -- id for right arrow
    KEY_DELETE           = 211,                            -- id for delete key
    KEY_HOME             = 199,                            -- id for home key
    KEY_END              = 207,                            -- id for end key

    ROLODEX              = "RolodexFrame",                 -- name of Rolodex's window
    
    DEBUG                = 0,                              -- enable debug mode (0 = no, 1 = yes)
}

MailWindow.TABS_MASSMAIL   = 4                             -- number of MassMail's tab
MailWindow.TABS_MAX_NUMBER = 4                             -- total number of tabs

zmmd = zMailModData                                        -- shortcut for zMailModData

zL = {}                                                    -- shortcut for localized strings