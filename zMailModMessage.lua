--[[
    --> file: zMailModMessage.lua
    --> file: zMailModMessage.xml

    --> description: redrawing Message window

    --> author: zoog
    --> email: ZooGTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

zMailModMessage = {}

mmv = zMailModMessage

local win = zmmd.WINDOW_MESSAGE

--------------------------------------------------------------------------------

function mmv.Initialize()
    mmv.UpdateLayout()
end


function mmv.UpdateLayout()
    if zmmd.LAYOUT_MESSAGE == 1 then
        mmv.SetNewLayout()
    else
        mmv.SetDefaultLayout()
    end
end


function mmv.SetNewLayout()
    local parent = win
    local title  = parent .. "TitleBar"
    local bgbar  = parent .. "BarBackground"
    local fromH  = parent .. "FromHeader"
    local fromT  = parent .. "FromText"
    local subT   = parent .. "SubjectText"
    local sep1   = parent .. "Separator1"
    local sep2   = parent .. "Separator2"
    local msg    = parent .. "Body"
    local items  = parent .. "AttachmentSlots"
    local moneyH = parent .. "AttachmentHeader"
    local moneyT = parent .. "CoinsFrame"
    local bg     = "zMailModMessageBackground"
    local topbg  = "zMailModMessageTopBackground"
    local topsep = "zMailModMessageTopSeparator"
    local subH   = "zMailModMessageSubjectHeader"
    local close  = "zMailModMessageCloseButton"
    local replyB = "zMailModMessageReplyButton"
    local takeB  = "zMailModMessageTakeItemButton"
    local delB   = "zMailModMessageDeleteButton"

    WindowSetShowing(title, false)
    WindowSetShowing(parent .. "Background", false)
    WindowSetShowing(parent .. "CornerImage", false)
    WindowSetShowing(parent .. "Close", false)
    WindowSetShowing(bgbar, false)
    WindowSetShowing(parent .. "CommandReplyButton", false)
    WindowSetShowing(parent .. "CommandTakeItemButton", false)
    WindowSetShowing(parent .. "DeleteMessageButton", false)

    if not DoesWindowExist(bg) then
        CreateWindowFromTemplate(bg, "zMailModBackgroundFull", parent)
        WindowAddAnchor(bg, "topleft", parent, "topleft", 0, 0)
        WindowAddAnchor(bg, "bottomright", parent, "bottomright", 0, 0)
        WindowSetTintColor(bg, 217, 191, 157)
    end
    WindowSetShowing(bg, true)
    
    if not DoesWindowExist(close) then
        CreateWindowFromTemplate(close, "zMailModMessageClose", parent)
    end
    WindowSetShowing(close, true)
    
    WindowClearAnchors (fromH)
    WindowAddAnchor    (fromH, "topleft", bg, "topleft", 10, 45)
    WindowSetDimensions(fromH, 103, 32)
    mmu.SetLabelFont   (fromH, "font_default_text_no_outline")
    LabelSetTextColor  (fromH, 50, 37, 22)

    WindowClearAnchors (fromT)
    WindowAddAnchor    (fromT, "topright", fromH, "topleft", 20, 0)
    WindowSetDimensions(fromT, 280, 32)
    mmu.SetLabelFont   (fromT, "font_default_text_no_outline")
    LabelSetTextColor  (fromT, 0, 16, 32)

    if not DoesWindowExist(subH) then
        CreateWindow   (subH, false)
        WindowSetParent(subH, parent)
        LabelSetText   (subH, GetMailString(StringTables.Mail.LABEL_MAIL_HEADER_SUBJECT))
    end
    WindowSetShowing(subH, true)

    WindowClearAnchors (subT)
    WindowAddAnchor    (subT, "topright", subH, "topleft", 20, 0)
    WindowSetDimensions(subT, 280, 32)
    mmu.SetLabelFont   (subT, "font_default_text_no_outline")
    LabelSetTextColor  (subT, 0, 16, 32)

    WindowClearAnchors (sep1)
    WindowAddAnchor    (sep1, "topleft", bg, "topleft", 0, 125)
    WindowAddAnchor    (sep1, "topright", bg, "topright", 0, 125)

    WindowClearAnchors (msg)
    WindowSetDimensions(msg, 356, 265)
    WindowAddAnchor    (msg, "bottomleft", sep1, "topleft", 30, 0)
    mmu.SetLabelFont   (msg .. "ScrollChildText", "font_default_text_no_outline")
    LabelSetTextColor  (msg .. "ScrollChildText", 0, 16, 32)

    WindowClearAnchors (moneyH)
    WindowAddAnchor    (moneyH, "topleft", items, "bottomleft", 0, 0)
    mmu.SetLabelFont   (moneyH, "font_default_text_no_outline")
    LabelSetTextAlign  (moneyH, "leftcenter")
    LabelSetTextColor  (moneyH, 50, 37, 22)

    WindowClearAnchors (moneyT)
    WindowAddAnchor    (moneyT, "topright", items, "bottomright", 0, 0)

    WindowClearAnchors (items)
    WindowAddAnchor    (items, "top", parent .. "BarBackground", "bottom", 0, -10)

    if not DoesWindowExist(replyB) then
        CreateWindow   (replyB, false)
        WindowSetParent(replyB, parent)
        ButtonSetText  (replyB, GetMailString(StringTables.Mail.BUTTON_MAIL_REPLY))
    end

    if not DoesWindowExist(takeB) then
        CreateWindow   (takeB, false)
        WindowSetParent(takeB, parent)
        ButtonSetText  (takeB, GetMailString(StringTables.Mail.BUTTON_MAIL_TAKE_ITEM))
    end
    
    if not DoesWindowExist(delB) then
        CreateWindow   (delB, false)
        WindowSetParent(delB, parent)
        ButtonSetText  (delB, GetMailString(StringTables.Mail.BUTTON_MAIL_DELETE))
    end
    
    WindowSetLayer(parent .. "AttachmentBackground", 0)
    
    WindowSetShowing(parent .. "DisplaySeperator", false)
    
    mmv.UpdateMoneyLabelColor()
    mmv.UpdateBottomBar()
end


function mmv.SetDefaultLayout()
    local parent = win
    local title  = parent .. "TitleBar"
    local bgbar  = parent .. "BarBackground"
    local fromH  = parent .. "FromHeader"
    local fromT  = parent .. "FromText"
    local subT   = parent .. "SubjectText"
    local sep1   = parent .. "Separator1"
    local sep2   = parent .. "Separator2"
    local msg    = parent .. "Body"
    local items  = parent .. "AttachmentSlots"
    local moneyH = parent .. "AttachmentHeader"
    local moneyT = parent .. "CoinsFrame"
    local bg     = "zMailModMessageBackground"
    local topbg  = "zMailModMessageTopBackground"
    local topsep = "zMailModMessageTopSeparator"
    local subH   = "zMailModMessageSubjectHeader"
    local close  = "zMailModMessageCloseButton"
    local replyB = "zMailModMessageReplyButton"
    local takeB  = "zMailModMessageTakeItemButton"
    local delB   = "zMailModMessageDeleteButton"

    WindowSetShowing(title, true)
    WindowSetShowing(parent .. "Background", true)
    WindowSetShowing(parent .. "CornerImage", true)
    WindowSetShowing(parent .. "Close", true)
    WindowSetShowing(bgbar, true)
    WindowSetShowing(parent .. "CommandReplyButton", true)
    WindowSetShowing(parent .. "CommandTakeItemButton", true)
    WindowSetShowing(parent .. "DeleteMessageButton", true)

    if DoesWindowExist(bg) then WindowSetShowing(bg, false) end
    if DoesWindowExist(close) then WindowSetShowing(close, false) end
    if DoesWindowExist(subH) then WindowSetShowing(subH, false) end
    if DoesWindowExist(replyB) then WindowSetShowing(replyB, false) end
    if DoesWindowExist(takeB) then WindowSetShowing(takeB, false) end
    if DoesWindowExist(delB) then WindowSetShowing(delB, false) end

    WindowClearAnchors (fromH)
    WindowAddAnchor    (fromH, "topleft", parent .."From", "topleft", 5, 15)
    WindowSetDimensions(fromH, 100, 32)
    mmu.SetLabelFont   (fromH, "font_journal_text")
    LabelSetTextColor  (fromH, 255, 255, 255)

    WindowClearAnchors (fromT)
    WindowAddAnchor    (fromT, "right", fromH, "left", 10, 0)
    WindowSetDimensions(fromT, 250, 32)
    mmu.SetLabelFont   (fromT, "font_journal_text")
    LabelSetTextColor  (fromT, 255, 255, 255)

    WindowClearAnchors (subT)
    WindowAddAnchor    (subT, "bottomleft", parent .."From", "topleft", 15, 15)
    WindowAddAnchor    (subT, "bottomright", parent .."From", "bottomright", -20, 47)
    WindowSetDimensions(subT, 250, 32)
    mmu.SetLabelFont   (subT, "font_journal_text")
    LabelSetTextColor  (subT, 255, 255, 255)

    WindowClearAnchors (sep1)
    WindowAddAnchor    (sep1, "bottomleft", subT, "topleft", 0, 0)
    WindowAddAnchor    (sep1, "bottomright", subT, "bottomright", 0, 20)

    WindowClearAnchors (msg)
    WindowAddAnchor    (msg, "bottomleft", sep1, "topleft", 10, 0)
    WindowAddAnchor    (msg, "topright", parent .. "DisplaySeperator", "bottomright", -15, 5)
    mmu.SetLabelFont   (msg .. "ScrollChildText", "font_journal_text")
    LabelSetTextColor  (msg .. "ScrollChildText", 255, 255, 255)

    WindowClearAnchors (moneyH)
    WindowAddAnchor    (moneyH, "left", moneyT, "right", 0, 0)
    mmu.SetLabelFont   (moneyH, "font_journal_text")
    LabelSetTextAlign  (moneyH, "right")
    LabelSetTextColor  (moneyH, 255, 255, 255)

    WindowClearAnchors (moneyT)
    WindowAddAnchor    (moneyT, "top", items, "bottomleft", 0, -5)

    WindowClearAnchors (items)
    WindowAddAnchor    (items, "top", bgbar, "bottom", 0, 4)

    WindowSetShowing(parent .. "DisplaySeperator", true)

    mmv.UpdateMoneyLabelColor()
    mmv.UpdateBottomBar()
end


function mmv.UpdateAttachment(data)
    zmm.hooked.MailWindowMessage_UpdateAttachment(data)
    mmv.UpdateMoneyLabelColor()
end


function mmv.UpdateMoneyLabelColor()
    local color = {r = 50, g = 37, b = 22}
    if zmmd.LAYOUT_MESSAGE == 0 then color = {r = 255, g = 255, b = 255} end
    
    mmu.SetLabelColor(win .. "CoinsFrameGoldText", color)
    mmu.SetLabelColor(win .. "CoinsFrameSilverText", color)
    mmu.SetLabelColor(win .. "CoinsFrameBrassText", color)
end


function mmv.UpdateBottomBar()
    local zReply  = "zMailModMessageReplyButton"
    local zTake   = "zMailModMessageTakeItemButton"
    local zDelete = "zMailModMessageDeleteButton"
    local dReply  = win .. "CommandReplyButton"
    local dTake   = win .. "CommandTakeItemButton"
    local dDelete = win .. "DeleteMessageButton"

    if zmmd.LAYOUT_MESSAGE == 1 then
        WindowSetShowing(zReply, true)
        -- #TODO: not working anymore
        WindowSetShowing(zTake, WindowGetShowing(dTake))
        WindowSetShowing(zDelete, true)
        WindowSetShowing(dReply, false)
        WindowSetShowing(dTake, false)
        WindowSetShowing(dDelete, false)
    end
end