--[[
    --> file: zMailModAuction.lua
    --> file: zMailModAuction.xml

    --> description: managing Auction window

    --> author: zoog
    --> email: ZooGTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

zMailModAuction = {}

mma = zMailModAuction

--------------------[VARIABLES]-------------------------------------------------

local win = zmmd.WINDOW_AUCTION

mma.checkbox = {}
mma.current  = {}
mma.attach   = {}
mma.lastID   = 0

--------------------[INITIALIZATION]--------------------------------------------

-- initialization
function mma.Initialize()
    zmm.UpdateLayout("Auction")
    mma.ResetAttachmentsInfo()
end


-- resets information about currently processed attachments
function mma.ResetAttachmentsInfo()
    mma.attach =
    {
        money = 0,
        item  = {icon = 0, count = 0, rarity = 0, name = L""}
    }
end


-- initialization of checkboxes
function mma.ClearCheckboxes()
    mma.checkbox = {}
    mma.Populate()
end


-- checking and unchecking checkboxes
function mma.SelectMail()
    local name  = SystemData.ActiveWindow.name
    local winID = WindowGetId(name)
    local id    = ListBoxGetDataIndex(win .. "List", winID)

    if mma.checkbox[id] ~= 0 and mma.checkbox[id] ~= nil then
        mma.checkbox[id] = nil
        ButtonSetPressedFlag("zMailModAuctionCheckbox" .. winID, false)
    else
        mma.checkbox[id] = MailWindowTabAuction.listData[id].messageID
        ButtonSetPressedFlag("zMailModAuctionCheckbox" .. winID, true)
    end
end


-- populating function
function mma.Populate()
    zmm.hooked.MailWindowAuction_Populate()

    if MailWindowTabAuctionList.PopulatorIndices ~= nil then
        for row, id in ipairs(MailWindowTabAuctionList.PopulatorIndices) do
            local label = "zMailModAuctionCOD" .. row
            if DoesWindowExist(label) then WindowSetShowing(label, false) end
            zmm.UpdateListRow("Auction", row, id)
        end
    end
end


-- populates row icon
function mma.PopulateItemIcon(row, data)
    zmm.hooked.MailWindowAuction_PopulateItemIcon(row, data)
    zmm.UpdateListRowIcon(row, data)
end


-- displays number of messages
function mma.HeadersNumber()
    zmm.hooked.MailWindowAuction_HeadersNumber()
    if #MailWindowTabAuction.listData ~= 1 then
        LabelSetText(win .. "MessageNumberText", #MailWindowTabAuction.listData .. L" " .. zL["MESSAGES"])
    else
        LabelSetText(win .. "MessageNumberText", #MailWindowTabAuction.listData .. L" " .. zL["MESSAGE"])
    end
end


-- selects all messages
function mma.SelectAll()
    local selected = mma.AllSelected()

    for i = 1, #MailWindowTabAuction.listDataOrder, 1 do
        if selected then
            mma.checkbox = {}
            break
        else
            mma.checkbox[i] = MailWindowTabAuction.listData[i].messageID
        end
    end

    mma.Populate()
end


-- selects messages containing money
function mma.SelectMoney()
    for i, id in pairs(MailWindowTabAuction.listDataOrder) do
        local data = MailWindowTabAuction.listData[id]

        if not data.isCOD and data.from == GetStringFromTable("AuctionHouseStrings", StringTables.AuctionHouse.MAIN_TITLE) and data.attachmentMoney > 0 and not data.isMoneyTaken then
            mma.checkbox[id] = MailWindowTabAuction.listData[id].messageID
        end
    end
    
    mma.Populate()
end


-- checks if all messages have been selected
function mma.AllSelected()
    for i = 1, #MailWindowTabAuction.listDataOrder, 1 do
        if mma.checkbox[i] == 0 or mma.checkbox[i] == nil then return false end
    end

    return true
end


-- Updates selected checkbox list
function mma.MailUpdated()
    mma.HeadersNumber()

    local mails = mma.GetMailsIdx()
    local cache = DataUtils.CopyTable(mma.checkbox)
    mma.checkbox = {}

    for i, id in pairs(cache) do
        if mails[id] ~= nil then
            mma.checkbox[mails[id]] = id
        end
    end

    mma.Populate()
end


-- corrects sentTimeStamp values of each mail to prevent opened mail going down on the list
function mma.HeaderUpdated(data, type)
    local id    = data[1].messageID
    local mails = mma.GetMailsIdx()

    if mails[id] ~= nil then
         local prev = mma.GetMailDataById(id).sentTimeStamp
         local curr = data[1].messageSendDelta * (-1)
         local time = curr - prev
         for i, mail in pairs(MailWindowTabAuction.listData) do
             MailWindowTabAuction.listData[i].sentTimeStamp = MailWindowTabAuction.listData[i].sentTimeStamp + time
         end
    end

    zmm.hooked.MailWindowAuction_HeaderUpdated(data, type)

    if id == mma.current.messageID then
        mma.current = mma.GetMailDataById(id)
    end
end


-- hides original coins icon
function mma.PopulateCoins(row, data)
    zmm.hooked.MailWindowAuction_PopulateCoins(row, data)
    if zmmd.LAYOUT_AUCTION ~= 0 then WindowSetShowing(row .. "CoinsImage", false) end
end


-- dummy
function mma.OnMouseOverCOD()
end


-- returns table with mails' list idx and messages idx
function mma.GetMailsIdx()
    local mails = {}

    for i = 1, #MailWindowTabAuction.listDataOrder, 1 do
        mails[MailWindowTabAuction.listData[i].messageID] = i
    end

    return mails
end


-- returns data of the mail with given id
function mma.GetMailDataById(id)
    local mails = mma.GetMailsIdx()
    return MailWindowTabAuction.listData[mails[id]]
end


-- returns data of the mail with given number (row number 1-7 or 1-5)
function mma.GetMailDataByNumber(num)
    local id = ListBoxGetDataIndex(win .. "List", num)
    return MailWindowTabAuction.listData[id]
end


-- sorts messages
function mma.SortLetters()
    local mails = {}

    for i, id in pairs(mma.checkbox) do
        table.insert(mails, i)
    end

    table.sort(mails)

    return mails
end


-- returns first non-COD message
function mma.GetNextLetter()
    local mails = mma.SortLetters()

    for i, id in pairs(mails) do
        local mail = MailWindowTabAuction.listData[id]
        if not mail.isCOD then return mail end
    end

    return false
end

--------------------[OPENING]---------------------------------------------------

-- starts opening job
function mma.OpenSelected()
    if zmmd.JOB == zmmd.IDLE then
        mmt.errors = 0
        zmmd.JOB = zmmd.OPENING
        zmmd.STATE = zmmd.AUCTION
        mma.Open()
    end
end


-- stops opening
function mma.AuctionStop()
    MailWindowTabMessage.OnClose()
    zmm.UpdateResults(89)
    mmt.errors = 0
    mma.current = {}
    zmmd.JOB = zmmd.IDLE
    zmmd.STATE = zmmd.READY
    mma.ClearCheckboxes()
    return false
end


-- opens mail
function mma.Open()
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.AUCTION then
        local mail = mma.GetNextLetter()
        if not mail then mma.AuctionStop() return end
        mma.current = mail
        SendMailboxCommand(MailWindow.MAILBOX_OPEN_MESSAGE, GameData.MailboxType.AUCTION, mail.messageID, L"", L"", L"", 0, {}, {}, false)
    end
end


-- takes attachments from mail
function mma.MailOpened(data, type)
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.AUCTION then
        MailWindowTabMessage.OnClose()
        if data.messageID == mma.current.messageID then
            if zmm.CanBeDeleted(mma.current) then
                mma.DeleteMail(mma.current, type)
            else
                if #mma.current.attachmentIconIDs > 0 and mma.current.attachmentsTakenTable[1] == false then
                    mma.SaveAttachmentsInfo(mma.current, data.itemsAttached[1].name, data.itemsAttached[1].stackCount, data.itemsAttached[1].rarity, data.itemsAttached[1].iconNum)
                else
                    mma.SaveAttachmentsInfo(mma.current, L"", 0, 0, 0)
                end
                SendMailboxCommand(MailWindow.MAILBOX_TAKE_ALL, GameData.MailboxType.AUCTION, data.messageID, L"", L"", L"", 0, {}, {}, false)
            end
        end
    end
end


-- saves information about attachmens in current message
function mma.SaveAttachmentsInfo(mail, name, count, rarity, icon)
    if mail.isCOD then return false end
    if mail.attachmentMoney == 0 and #mail.attachmentIconIDs == 0 then return false end
    if mail.attachmentMoney > 0 and not mail.isMoneyTaken then mma.attach.money = mail.money end
    if icon > 0 and not mail.attachmentsTakenTable[1] then mma.attach.item = {icon = icon, count = count, rarity = rarity, name = name} end
end


-- deletes mail
function mma.DeleteMail(data, type)
    if zmm.CanBeDeleted(data) then
        local logData =
        {
            sender  = data.from,
            subject = data.subject,
            type    = GetMailString(MailWindow.Tabs[MailWindow.TABS_AUCTION].label),
            money   = mma.attach.money,
            item    = L"" .. mma.attach.item.icon .. L"#" .. mma.attach.item.count .. L"#" .. mma.attach.item.rarity .. L"#" .. mma.attach.item.name
        }
        
        if mma.lastID ~= data.messageID then
            mma.lastID = data.messageID
            mml.AddEntry(logData)
        end
        
        SendMailboxCommand(MailWindow.MAILBOX_DELETE_MESSAGE, GameData.MailboxType.AUCTION, data.messageID, L"", L"", L"", 0, {}, {}, false)
    else
        mma.Open()
    end
end


-- continues work after mail has been deleted
function mma.MailDeleted(id, type)
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.AUCTION then
        mma.current = {}
        mma.MailUpdated()
        mma.Open()
    end
end


-- event handler for changes in user's inventory
function mma.ItemsUpdated()
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.AUCTION then
        mma.DeleteMail(mma.current, GameData.MailboxType.AUCTION)
    end
end


-- event handler for changes in user's money
function mma.MoneyUpdated()
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.AUCTION then
        mma.DeleteMail(mma.current, GameData.MailboxType.AUCTION)
    end
end