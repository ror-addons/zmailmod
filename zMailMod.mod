<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="zMailMod" version="0.9.9.6" date="28/08/2010" >
        
        <Author name="zoog" email="ZoogTheOrc@gmail.com" />
        
        <Description text="zMailMod enhances the default mail system by adding several options to the mail window." />

        <VersionSettings gameVersion="1.3.6" />
        
        <Dependencies>
            <Dependency name="EASystem_Utils" />
            <Dependency name="EA_ChatWindow"/>
            <Dependency name="EA_MailWindow" />
            <Dependency name="EA_BackpackWindow" />
        </Dependencies>

        <Files>
            <File name="zMailModData.lua" />
            <File name="Languages\english.lua" />
            <File name="Languages\russian.lua" />
            <File name="Languages\italian.lua" />
            <File name="Languages\french.lua" />
            <File name="Languages\s_chinese.lua" />
            <File name="Languages\german.lua" />
            <File name="Languages\japanese.lua" />
            <File name="Languages\t_chinese.lua" />
            <File name="Languages\spanish.lua" />
            <File name="Languages\korean.lua" />
            <File name="zMailMod.lua" />
            <File name="zMailModUtils.lua" />
            <File name="zMailModTemplates.xml" />
            <File name="zMailModMassMail.xml" />
            <File name="zMailModMassMail.lua" />
            <File name="zMailModInbox.xml" />
            <File name="zMailModinbox.lua" />
            <File name="zMailModAuction.xml" />
            <File name="zMailModAuction.lua" />
            <File name="zMailModSend.lua" />
            <File name="zMailModSend.xml" />
            <File name="zMailModMessage.lua" />
            <File name="zMailModMessage.xml" />
            <File name="zMailModTimer.lua" />
            <File name="zMailModLog.lua" />
            <File name="zMailModLog.xml" />
            <File name="zMailModOptions.lua" />
            <File name="zMailModOptions.xml" />
        </Files>

        <SavedVariables>
            <SavedVariable name="zMailModSavedSettings" />
        </SavedVariables>

        <OnInitialize>
            <CallFunction name="zMailMod.Initialize" />
        </OnInitialize>

        <OnUpdate>
            <CallFunction name="MassMailTimer.OnUpdate"/>
        </OnUpdate>

        <OnShutdown>
            <CallFunction name="zMailMod.Shutdown"/>
        </OnShutdown>

    </UiMod>
</ModuleFile>