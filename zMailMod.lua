--[[
    --> file: zMailMod.lua
    
    --> description: main and hooked functions
    
    --> author: zoog
    --> email: ZoogTheOrc@gmail.com
--]]

--------------------------------------------------------------------------------

zMailMod = {}

zmm = zMailMod

--------------------[HOOKS]-----------------------------------------------------

zmm.hooked = zmmd.HOOKED

zmm.hooked.MailWindow_Close                   = MailWindow.OnClose
zmm.hooked.MailWindow_HideTabAllElements      = MailWindow.HideTabAllElements
zmm.hooked.MailWindow_HideTabCurrElements     = MailWindow.HideTabCurrentElements
zmm.hooked.MailWindow_OnLButtonUpTab          = MailWindow.OnLButtonUpTab
zmm.hooked.MailWindow_OnResultsUpdated        = MailWindow.OnResultsUpdated
zmm.hooked.Backpack_LButtonUp                 = EA_Window_Backpack.InventoryLButtonUp
zmm.hooked.Backpack_RButtonUp                 = EA_Window_Backpack.InventoryRButtonUp
zmm.hooked.Backpack_LButtonDown               = EA_Window_Backpack.InventoryLButtonDown
zmm.hooked.MailWindowInbox_HeadersNumber      = MailWindowTabInbox.NumberOfHeadersChanged
zmm.hooked.MailWindowInbox_HeaderUpdated      = MailWindowTabInbox.OnHeaderUpdated
zmm.hooked.MailWindowInbox_Populate           = MailWindowTabInbox.Populate
zmm.hooked.MailWindowInbox_PopulateItemIcon   = MailWindowTabInbox.PopulateItemAttachments
zmm.hooked.MailWindowInbox_PopulateCoins      = MailWindowTabInbox.PopulateCoinsAttachment
zmm.hooked.MailWindowAuction_HeadersNumber    = MailWindowTabAuction.NumberOfHeadersChanged
zmm.hooked.MailWindowAuction_HeaderUpdated    = MailWindowTabAuction.OnHeaderUpdated
zmm.hooked.MailWindowAuction_Populate         = MailWindowTabAuction.Populate
zmm.hooked.MailWindowAuction_PopulateItemIcon = MailWindowTabAuction.PopulateItemAttachment
zmm.hooked.MailWindowAuction_PopulateCoins    = MailWindowTabAuction.PopulateCoinsAttachment
zmm.hooked.MailWindowSend_LButtonUpCOD        = MailWindowTabSend.OnLButtonUpCOD
zmm.hooked.MailWindowSend_LButtonUpSend       = MailWindowTabSend.OnLButtonUpSendButton
zmm.hooked.MailWindowSend_ClearEntries        = MailWindowTabSend.ClearEntries
zmm.hooked.MailWindow_SetListRowTints         = MailWindow.SetListRowTints
zmm.hooked.MailWindowMessage_UpdateAttachment = MailWindowTabMessage.UpdateMessageAttachments

--------------------[INITIALIZATION]--------------------------------------------

-- initialization
function zmm.Initialize()
    zmm.GetLocale()
    zmm.SetLanguage()
    zmm.RegisterEventsHandlers()
    zmm.GetPlayerName()
    zmm.PrintInitMessage()
    zmm.SetMassMailTabData()
    zmm.HookFunctions()
    zmm.LoadSettings()
    zmm.UpdateAnchor()
    mmw.Initialize()
    mmt.Initialize()
    mmi.Initialize()
    mms.Initialize()
    mma.Initialize()
    mml.Initialize()
    mmo.Initialize()
    mmv.Initialize()
    zmm.CreateTitleBarButtons()
    zmm.CreateDummyLabel()
end


-- registers event handlers
function zmm.RegisterEventsHandlers()
    RegisterEventHandler(SystemData.Events.MAILBOX_UNREAD_COUNT_CHANGED, "zmm.MailCount")
    RegisterEventHandler(SystemData.Events.INTERACT_MAILBOX_OPEN, "zmm.UpdateDisplay")
    WindowRegisterCoreEventHandler("EA_Window_OverheadMapMailNotificationIcon", "OnMouseOver", "zmm.OnMouseOverMailIcon")
    
    RegisterEventHandler(SystemData.Events.MAILBOX_MESSAGE_OPENED, "zmm.MailOpened")
    RegisterEventHandler(SystemData.Events.PLAYER_MONEY_UPDATED, "zmm.InventoryUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED,"zmm.InventoryUpdated")
	RegisterEventHandler(SystemData.Events.MAILBOX_HEADERS_UPDATED, "zmm.MailUpdated")
end


-- gets the game language
function zmm.GetLocale()
    zmmd.LANGUAGE = SystemData.Settings.Language.active
end


-- sets the language same as in game
function zmm.SetLanguage()
    zL = zmmd.LANGUAGES[zmmd.LANGUAGE]
end


-- adds MassMail data to MailWindow
function zmm.SetMassMailTabData()
    MailWindow.Tabs[MailWindow.TABS_MASSMAIL] =
    {
        name               = zmmd.TAB_MASSMAIL,
        label              = 0,
        filterWindow       = "zMailModMassMailFilter",
        window             = zmmd.WINDOW_MASSMAIL,
        tooltip            = StringTables.Mail.TOOLTIP_MAIL_TAB_SEND,
        populationFunction = nil,
    }
end


-- hooks
function zmm.HookFunctions()
    MailWindow.OnClose                              = zmm.MailClose
    MailWindow.HideTabAllElements                   = zmm.HideTabAllElements
    MailWindow.HideTabCurrentElements               = zmm.HideTabCurrElements
    MailWindow.OnLButtonUpTab                       = zmm.OnLButtonUpTab
    MailWindow.OnResultsUpdated                     = zmm.UpdateResults
    EA_Window_Backpack.InventoryLButtonUp           = zmm.InventoryLButtonUp
    EA_Window_Backpack.InventoryRButtonUp           = zmm.InventoryRButtonUp
    EA_Window_Backpack.InventoryLButtonDown         = zmm.InventoryLButtonDown
    MailWindowTabInbox.NumberOfHeadersChanged       = mmi.HeadersNumber
    MailWindowTabInbox.OnHeaderUpdated              = mmi.HeaderUpdated
    MailWindowTabInbox.Populate                     = mmi.Populate
    MailWindowTabInbox.PopulateItemAttachments      = mmi.PopulateItemIcon
    MailWindowTabInbox.PopulateCoinsAttachment      = mmi.PopulateCoins
    MailWindowTabAuction.NumberOfHeadersChanged     = mma.HeadersNumber
    MailWindowTabAuction.OnHeaderUpdated            = mma.HeaderUpdated
    MailWindowTabAuction.Populate                   = mma.Populate
    MailWindowTabAuction.PopulateItemAttachment     = mma.PopulateItemIcon
    MailWindowTabAuction.PopulateCoinsAttachment    = mma.PopulateCoins
    MailWindowTabSend.OnLButtonUpCOD                = mms.OnLButtonUpCOD
    MailWindowTabSend.OnLButtonUpSendButton         = mms.OnLButtonUpSend
    MailWindow.SetListRowTints                      = zmm.SetListRowTints
    MailWindowTabMessage.UpdateMessageAttachments   = mmv.UpdateAttachment
    MailWindowTabSend.ClearEntries                  = mms.ClearEntries
end


-- changes mail window's anchor
function zmm.UpdateAnchor()
    WindowClearAnchors("MailWindow")
    WindowAddAnchor("MailWindow", "topright", "Root", "topright", -50, 100)
end


-- adds buttons to the Mail Window's title bar
function zmm.CreateTitleBarButtons()
    local set = "zMailModSettingsButton"
    local log = "zMailModLogButton"
    
    CreateWindow(set, true)
    WindowSetParent(set, "MailWindow")
    ButtonSetText(set .. "Active", L"z")

    CreateWindow(log, true)
    WindowSetParent(log, "MailWindow")
    ButtonSetText(log .. "Active", L"L")
end

--------------------[SAVED SETTINGS]--------------------------------------------

-- validates the values of saves variables
function zmm.ValidateSettings()
    local player = zmmd.PLAYER_NAME

    if not zMailModSavedSettings then zMailModSavedSettings = {} end
    if zMailModSavedSettings[player] == nil then zMailModSavedSettings[player] = {} end
    
    -- check version number and remove deprecated variables
    if zMailModSavedSettings.version ~= zmmd.VERSION then
        zMailModSavedSettings.version = zmmd.VERSION
        
        if zMailModSavedSettings[player].autoCompletion ~= nil then zMailModSavedSettings[player].autoCompletion = nil end
        if zMailModSavedSettings[player].autoCompletionSend ~= nil then zMailModSavedSettings[player].autoCompletionSend = nil end
        if zMailModSavedSettings[player].autoCompletionMassMail ~= nil then zMailModSavedSettings[player].autoCompletionMassMail = nil end
        if zMailModSavedSettings[player].background ~= nil then zMailModSavedSettings[player].background = nil end
        if zMailModSavedSettings[player].bags ~= nil then zMailModSavedSettings[player].bags = nil end
        if zMailModSavedSettings[player].extraButtons ~= nil then zMailModSavedSettings[player].extraButtons = nil end
        if zMailModSavedSettings[player].itemName ~= nil then zMailModSavedSettings[player].itemName = nil end
        if zMailModSavedSettings[player].itemCount ~= nil then zMailModSavedSettings[player].itemCount = nil end
    end
        
    if zMailModSavedSettings[player].autocomplete == nil or zMailModSavedSettings[player].autocomplete < 0 or zMailModSavedSettings[player].autocomplete > 1 then
        zMailModSavedSettings[player].autocomplete = zmmd.DEFAULT.AUTOCOMPLETE
    end

    if zMailModSavedSettings[player].bagsOpen == nil or zMailModSavedSettings[player].bagsOpen < 0 or zMailModSavedSettings[player].bagsOpen > 1 then
        zMailModSavedSettings[player].bagsOpen = zmmd.DEFAULT.BAGS_OPEN
    end

    if zMailModSavedSettings[player].bagsClose == nil or zMailModSavedSettings[player].bagsClose < 0 or zMailModSavedSettings[player].bagsClose > 1 then
        zMailModSavedSettings[player].bagsClose = zmmd.DEFAULT.BAGS_CLOSE
    end
    
    if zMailModSavedSettings[player].expire == nil or zMailModSavedSettings[player].expire < 0 then
        zMailModSavedSettings[player].expire = zmmd.DEFAULT.EXPIRE_WARNING
    end

    if zMailModSavedSettings[player].key == nil or (zMailModSavedSettings[player].key ~= 4 and zMailModSavedSettings[player].key ~= 8 and zMailModSavedSettings[player].key ~= 32 and zMailModSavedSettings[player].key ~= 0) then
        zMailModSavedSettings[player].key = zmmd.DEFAULT.KEY
    end

    if zMailModSavedSettings[player].mouse == nil or (zMailModSavedSettings[player].mouse ~= 1 and zMailModSavedSettings[player].mouse ~= 2) then
        zMailModSavedSettings[player].mouse = zmmd.DEFAULT.MOUSE
    end

    if zMailModSavedSettings[player].errors == nil or zMailModSavedSettings[player].errors < 0 then
        zMailModSavedSettings[player].errors = zmmd.DEFAULT.ERRORS
    end

    if zMailModSavedSettings[player].pauseTime == nil or zMailModSavedSettings[player].pauseTime < 0 then
        zMailModSavedSettings[player].pauseTime = zmmd.DEFAULT.PAUSE_TIME
    end

    if zMailModSavedSettings[player].extraCooldown == nil or zMailModSavedSettings[player].extraCooldown < 0 then
        zMailModSavedSettings[player].extraCooldown = zmmd.DEFAULT.EXTRA_COOLDOWN
    end

    if zMailModSavedSettings[player].layoutInbox == nil or zMailModSavedSettings[player].layoutInbox < 0 or zMailModSavedSettings[player].layoutInbox > 2 then
        zMailModSavedSettings[player].layoutInbox = zmmd.DEFAULT.LAYOUT_INBOX
    end
    
    if zMailModSavedSettings[player].layoutSend == nil or zMailModSavedSettings[player].layoutSend < 0 or zMailModSavedSettings[player].layoutSend > 1 then
        zMailModSavedSettings[player].layoutSend = zmmd.DEFAULT.LAYOUT_SEND
    end

    if zMailModSavedSettings[player].layoutMessage == nil or zMailModSavedSettings[player].layoutMessage < 0 or zMailModSavedSettings[player].layoutMessage > 1 then
        zMailModSavedSettings[player].layoutMessage = zmmd.DEFAULT.LAYOUT_MESSAGE
    end

    if zMailModSavedSettings[player].MassAdd == nil or zMailModSavedSettings[player].MassAdd < 0 or zMailModSavedSettings[player].MassAdd > 1 then
        zMailModSavedSettings[player].MassAdd = zmmd.DEFAULT.MASSADD_ENABLE
    end
    
    if zMailModSavedSettings[player].lastRecipient == nil or zMailModSavedSettings[player].lastRecipient < 0 or zMailModSavedSettings[player].lastRecipient > 1 then
        zMailModSavedSettings[player].lastRecipient = zmmd.DEFAULT.LAST_RECIPIENT
    end
end


-- loads saved settings
function zmm.LoadSettings()
    local player = zmmd.PLAYER_NAME

    zmm.ValidateSettings()

    zmmd.AUTOCOMPLETE   = zMailModSavedSettings[player].autocomplete
    zmmd.BAGS_OPEN      = zMailModSavedSettings[player].bagsOpen
    zmmd.BAGS_CLOSE     = zMailModSavedSettings[player].bagsClose
    zmmd.EXPIRE_WARNING = zMailModSavedSettings[player].expire
    zmmd.KEY            = zMailModSavedSettings[player].key
    zmmd.MOUSE          = zMailModSavedSettings[player].mouse
    zmmd.ERRORS         = zMailModSavedSettings[player].errors
    zmmd.PAUSE_TIME     = zMailModSavedSettings[player].pauseTime
    zmmd.EXTRA_COOLDOWN = zMailModSavedSettings[player].extraCooldown
    zmmd.LAYOUT_INBOX   = zMailModSavedSettings[player].layoutInbox
    zmmd.LAYOUT_SEND    = zMailModSavedSettings[player].layoutSend
    zmmd.LAYOUT_MESSAGE = zMailModSavedSettings[player].layoutMessage
    zmmd.MASSADD_ENABLE = zMailModSavedSettings[player].MassAdd
    zmmd.LAST_RECIPIENT = zMailModSavedSettings[player].lastRecipient
end


-- saves settings
function zmm.SaveSettings()
    local player = zmmd.PLAYER_NAME
    
    zMailModSavedSettings[player].autocomplete  = zmmd.AUTOCOMPLETE
    zMailModSavedSettings[player].bagsOpen      = zmmd.BAGS_OPEN
    zMailModSavedSettings[player].bagsClose     = zmmd.BAGS_CLOSE
    zMailModSavedSettings[player].expire        = zmmd.EXPIRE_WARNING
    zMailModSavedSettings[player].key           = zmmd.KEY
    zMailModSavedSettings[player].mouse         = zmmd.MOUSE
    zMailModSavedSettings[player].errors        = zmmd.ERRORS
    zMailModSavedSettings[player].pauseTime     = zmmd.PAUSE_TIME
    zMailModSavedSettings[player].extraCooldown = zmmd.EXTRA_COOLDOWN
    zMailModSavedSettings[player].layoutInbox   = zmmd.LAYOUT_INBOX
    zMailModSavedSettings[player].layoutSend    = zmmd.LAYOUT_SEND
    zMailModSavedSettings[player].layoutMessage = zmmd.LAYOUT_MESSAGE
    zMailModSavedSettings[player].MassAdd       = zmmd.MASSADD_ENABLE
    zMailModSavedSettings[player].lastRecipient = zmmd.LAST_RECIPIENT
end

--------------------[INPUT HANDLING]--------------------------------------------

-- LMB (down)
function zmm.InventoryLButtonDown (buttonId, flags)
    local slot = EA_Window_Backpack.GetSlotFromActionButtonGroup(SystemData.ActiveWindow.name, buttonId)

    mmw.FixRecipient()

    if zmm.IsActive(zmmd.WINDOW_MASSMAIL) and zmm.AreButtonsPressed(1, flags) then
        mmw.QueueAddItem(0, slot, EA_BackpackUtilsMediator.GetCurrentBackpackType())
    else
        zmm.hooked.Backpack_LButtonDown(buttonId, flags)
    end
end


-- LMB (up)
function zmm.InventoryLButtonUp (buttonId, flags)
    local slot = EA_Window_Backpack.GetSlotFromActionButtonGroup(SystemData.ActiveWindow.name, buttonId)

    mmw.FixRecipient()

    if zmm.IsActive(zmmd.WINDOW_MASSMAIL) and zmm.AreButtonsPressed(1, flags) then
        mmw.QueueAddItem(0, slot, EA_BackpackUtilsMediator.GetCurrentBackpackType())
    else
        zmm.hooked.Backpack_LButtonUp(buttonId, flags)
    end
end


-- RMB
function zmm.InventoryRButtonUp (buttonId, flags)
    local slot = EA_Window_Backpack.GetSlotFromActionButtonGroup(SystemData.ActiveWindow.name, buttonId)

    mmw.FixRecipient()

    if zmm.IsActive(zmmd.WINDOW_MASSMAIL) and zmm.AreButtonsPressed(2, flags) then
        mmw.QueueAddItem(0, slot, EA_BackpackUtilsMediator.GetCurrentBackpackType())
    else
        zmm.hooked.Backpack_RButtonUp(buttonId, flags)
    end
end

------------------[MASSMAIL TAB]------------------------------------------------

-- hides current tab
function zmm.HideTabCurrElements()
    zmm.HideMassMailWinElements()
    zmm.hooked.MailWindow_HideTabCurrElements()

    if MailWindow.SelectedTab == MailWindow.TABS_MASSMAIL and not WindowGetShowing(zmmd.WINDOW_MASSMAIL) then zmm.HideMassMailWinElements() end
end


-- hides all tabs
function zmm.HideTabAllElements()
	zmm.HideMassMailWinElements()
    zmm.hooked.MailWindow_HideTabAllElements()
end


-- hides MassMail window
function zmm.HideMassMailWinElements()
    if DoesWindowExist(zmmd.WINDOW_MASSMAIL) then
	   WindowSetShowing(zmmd.WINDOW_MASSMAIL, false)
    end
end


-- shows MassMail window
function zmm.ShowMassMailWinElements()
	zmm.HideTabAllElements()
    WindowSetShowing(zmmd.WINDOW_MASSMAIL, true)
    MailWindow.SelectedTab = MailWindow.TABS_MASSMAIL
    MailWindow.SetHighlightedTabText(MailWindow.SelectedTab)
end


-- LMB pressed on one of the tabs in MailWindow
function zmm.OnLButtonUpTab()
    zmm.hooked.MailWindow_OnLButtonUpTab()

    local id = WindowGetId(SystemData.ActiveWindow.name)

    zmm.UpdateBottomBar()

    if id == MailWindow.TABS_MASSMAIL then
        mmw.LoadRecipient()
        mmw.FixRecipient()
    end

    if id == MailWindow.TABS_SEND then
        mms.LoadRecipient()
        LabelSetText("MailWindowTabSendAttachmentHeader", zL["LABEL_MONEY"])
    end
end

------------------[HOOKED FUNCTIONS]--------------------------------------------

-- shows various messages in result bar
function zmm.UpdateResults (resultCode, mailboxType)
    if zmm.IsActive(zmmd.WINDOW_MASSMAIL) then
         local label = MailWindow.Tabs[MailWindow.TABS_MASSMAIL].window .. "ResultText"

        if resultCode == nil or resultCode <= 0 then LabelSetText(label, L"") end

        WindowSetShowing(label, true)

        if resultCode == 4 then
            -- #TODO: put information about succesfull sending to debug window
        elseif resultCode == 90 then LabelSetText(label, zL["TEXT_MAIL_RESULT90"])
        elseif resultCode == 91 then LabelSetText(label, zL["TEXT_MAIL_RESULT91"])
        elseif resultCode == 92 then LabelSetText(label, zL["TEXT_MAIL_RESULT92"])
        elseif resultCode == 96 then LabelSetText(label, zL["TEXT_MAIL_RESULT96"] .. L" " .. zL["TEXT_MAIL_RESULT97"])
        elseif resultCode == 87 then
            local text = zL["TEXT_MAIL_RESULT98"]
            if zmmd.ERRORS > 0 then text = text .. L" " .. zL["RETRY"]:gsub(L"<<1>>", mmt.resultCounter) end
            LabelSetText(label, text)
        elseif resultCode == 97 then
            local text = zL["TEXT_MAIL_RESULT97"]:gsub(L"<<1>>", zmmd.ITEMS_SENT)
            text = text:gsub(L"<<2>>", zmmd.ITEMS_TO_SEND + zmmd.ITEMS_SENT)
            text = text:gsub(L"<<3>>", mmt.counter)
            LabelSetText(label, text)
        elseif resultCode == 98 then
            LabelSetText(label, zL["TEXT_MAIL_RESULT98"])
            mmw.ClearQueue()
            mmw.LoadDefaultSettings()
            mmw.QueueInitialize()
        elseif resultCode == 99 then
            LabelSetText(label, L"" .. zmmd.ITEMS_SENT .. zL["TEXT_MAIL_RESULT99"]:sub(6))
            mmw.ClearQueue()
            mmw.LoadDefaultSettings()
            mmw.QueueInitialize()
        else
            zmm.hooked.MailWindow_OnResultsUpdated(resultCode, mailboxType)
        end
    elseif zmm.IsActive("MailWindowTabInbox") or zmm.IsActive("MailWindowTabAuction") then
        local label = MailWindow.Tabs[MailWindow.SelectedTab].window .. "ResultText"

        WindowSetShowing(label, true)

        if resultCode == 12 then
            local mails = 0
            
            if zmmd.STATE == zmmd.INBOX then
                for i, id in pairs(mmi.checkbox) do
                    if type(i) == "number" then mails = mails + 1 end
                end
            elseif zmmd.STATE == zmmd.AUCTION then
                for i, id in pairs(mma.checkbox) do
                    if type(i) == "number" then mails = mails + 1 end
                end
            end
            
            if mails == 1 then
                LabelSetText(label, zL["TEXT_MAIL_RESULT93"])
            else
                local text = zL["TEXT_MAIL_RESULT94"]:gsub(L"<<2>>", L"" .. mails - 1)
                LabelSetText(label, text)
            end
        elseif resultCode == 16 then WindowSetShowing(label, false)
        elseif resultCode == 88 then LabelSetText(label, zL["TEXT_MAIL_RESULT95"] .. L" " .. zL["TEXT_MAIL_RESULT88"])
        elseif resultCode == 89 then LabelSetText(label, zL["TEXT_MAIL_RESULT89"])
        elseif resultCode == 95 then LabelSetText(label, zL["TEXT_MAIL_RESULT87"] .. L" " .. zL["TRY_LATER"])
        else
            zmm.hooked.MailWindow_OnResultsUpdated(resultCode, mailboxType)
        end
    else
        zmm.hooked.MailWindow_OnResultsUpdated(resultCode, mailboxType)
    end
end


-- closes MailWindow
function zmm.MailClose()
    mmo.Hide()
    zmm.UpdateResults()
    mmw.ClearEntries()
    mmi.ClearCheckboxes()
    mmi.InboxStop()
    mma.AuctionStop()
    mmw.HideCraftingList()
    mmw.HideRarityList()
    zmm.CloseBackpack()
    zmm.hooked.MailWindow_Close()
end

--------------------[MAP MAIL ICON]---------------------------------------------

-- sets up tooltip for map mail icon with numbers of unread mails
function zmm.OnMouseOverMailIcon()
    local text = L""

    if zmmd.UNREAD_INBOX > 0 then
        text = text .. GetStringFormatFromTable("MailStrings", StringTables.Mail.TEXT_YOU_HAVE_X_UNREAD_MESSAGES_IN_YOUR_MAILBOX, {L"" .. zmmd.UNREAD_INBOX})
    end

    if zmmd.UNREAD_AUCTION > 0 then
        if text ~= L"" then text = text .. L"<p>" end
        text = text .. GetStringFormatFromTable("MailStrings", StringTables.Mail.TEXT_YOU_HAVE_X_UNREAD_MESSAGES_IN_AUCTION_BOX, {L"" .. zmmd.UNREAD_AUCTION})
    end

    Tooltips.CreateTextOnlyTooltip ("EA_Window_OverheadMapMailNotificationIcon", text)
    Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)
end


-- sets up numbers of unread mails for the tooltip
function zmm.MailCount(type, count)
    if type == GameData.MailboxType.PLAYER then
        zmmd.UNREAD_INBOX = count
    elseif type == GameData.MailboxType.AUCTION then
        zmmd.UNREAD_AUCTION = count
    end

    if zmmd.UNREAD_INBOX == 0 and zmmd.UNREAD_AUCTION == 0 then
        WindowSetShowing("EA_Window_OverheadMapMailNotificationIcon", false)
    else
        WindowSetShowing("EA_Window_OverheadMapMailNotificationIcon", true)
    end
end

--------------------[MISCELLANEOUS]---------------------------------------------

-- prints init message
function zmm.PrintInitMessage()
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, zL["INIT_MESSAGE"])
end


-- gets the player name (lowercase)
function zmm.GetPlayerName()
    local name
    name = GameData.Player.name
    name = WStringToString(name)
    name = string.sub(name, 1, -3)
    name = string.lower(name)
    zmmd.PLAYER_NAME = name
end


-- opens backpack
function zmm.OpenBackpack()
    if zmmd.BAGS_OPEN == 1 then
        if not WindowGetShowing(EA_Window_Backpack.windowName) then
            BroadcastEvent(SystemData.Events.TOGGLE_BACKPACK_WINDOW)
        end
    end
end


-- closes backpack
function zmm.CloseBackpack()
    if zmmd.BAGS_CLOSE == 1 then
        if WindowGetShowing(EA_Window_Backpack.windowName) then
            BroadcastEvent(SystemData.Events.TOGGLE_BACKPACK_WINDOW)
        end
    end
end


-- checks if window given exists and is visible
function zmm.IsActive (window)
    if not WindowGetShowing("MailWindow") then return false end
    if not DoesWindowExist(window) then return false end
    if not WindowGetShowing(window) then return false end
    return true
end


-- checks if right keys are pressed
function zmm.AreButtonsPressed (mouseButton, flags)
    if mouseButton ~= zmmd.MOUSE then
        return false
    end

    if flags ~= zmmd.KEY then
        return false
    end

    return true
end


-- returns a table with color of rarity given
function zmm.GetRarityColor (num)
    local r = GameDefs.ItemRarity[num].color.r
    local g = GameDefs.ItemRarity[num].color.g
    local b = GameDefs.ItemRarity[num].color.b
    return {r = r, g = g, b = b}
end


-- closes extra windows
function zmm.Shutdown()
    mmo.Hide()
    mml.Hide()
end


-- manages showing extra windows duiring Mail Window opening
function zmm.UpdateDisplay()
    zmm.UpdateBottomBar()
    zmm.UpdateResults()
    zmm.OpenBackpack()
    mmw.LoadRecipient()
    mms.LoadRecipient()
    zmm.RegisterRolodex()
end


-- creates invisible label needed for names autocompletion
function zmm.CreateDummyLabel()
    CreateWindow("zMailModDummy", false)
end


function zmm.SetRowsNumber(type)
    if type == nil then
        zmm.SetRowsNumber("Inbox")
        zmm.SetRowsNumber("Auction")
    end
    
    local list   = "MailWindowTab" .. type .. "List"
    local height = 64
    local num    = 7

    if zmmd.LAYOUT_INBOX == 0 then
        height = 90
        num    = 5
    end
    
    ListBoxSetVisibleRowCount(list, num)
    
    for i = 1, num, 1 do
        local row   = list .. "Row" .. i
        WindowSetDimensions(row , 480, height)
        WindowSetDimensions(row .. "Header", 450, height)
    end
end


function zmm.SetListRowTints()
    if MailWindow.SelectedTab ~= MailWindow.TABS_INBOX and MailWindow.SelectedTab ~= MailWindow.TABS_AUCTION then return end
    zmm.hooked.MailWindow_SetListRowTints()
    if zmmd.LAYOUT_INBOX == 1 then zmm.HideListRowTints() end
end


function zmm.HideListRowTints()
    if MailWindow.SelectedTab ~= MailWindow.TABS_INBOX and MailWindow.SelectedTab ~= MailWindow.TABS_AUCTION then return end

    -- using inbox only, auction tab list has the same number of rows
    for i = 1, MailWindowTabInboxList.numVisibleRows, 1 do
        local row = MailWindow.Tabs[MailWindow.SelectedTab].window .. "ListRow" .. i .. "RowBackground"
        WindowSetAlpha(row, 0)
    end
end


function zmm.SelectMail()
    if MailWindow.SelectedTab == MailWindow.TABS_INBOX then
        mmi.SelectMail()
    elseif MailWindow.SelectedTab == MailWindow.TABS_AUCTION then
        mma.SelectMail()
    end
end


function zmm.UpdateListRow(type, i, id)
    local row      = "MailWindowTab" .. type .. "ListRow" .. i
    local header   = row .. "Header"
    local item     = row .. "AttachmentButtonIcon"
    local expire   = header .. "ExpiresText"
    local checkbox = "zMailMod" .. type .. "Checkbox" .. i
    local time     = 0
    local isSet    = false
    local isCOD    = false
    local current  = zmmd.LAYOUT_INBOX

    if type == "Inbox" then
        time     = tonumber(MailWindowTabInbox.listData[id].expiresTimeStamp)
        isSet    = mmi.checkbox[id] ~= nil and mmi.checkbox[id] ~= 0
        isCOD    = MailWindowTabInbox.listData[id].isCOD and not MailWindowTabInbox.listData[id].isCODPaid
        zmm.UpdateListRowLabels(header, MailWindowTabInbox.listData[id].hasBeenRead)
    elseif type == "Auction" then
        time     = tonumber(MailWindowTabAuction.listData[id].expiresTimeStamp)
        isSet    = mma.checkbox[id] ~= nil and mma.checkbox[id] ~= 0
        isCOD    = MailWindowTabAuction.listData[id].isCOD and not MailWindowTabAuction.listData[id].isCODPaid
        zmm.UpdateListRowLabels(header, MailWindowTabAuction.listData[id].hasBeenRead)
    end

    if DoesWindowExist(checkbox) then ButtonSetPressedFlag(checkbox, isSet) end
    LabelSetText(expire, mmu.FormatExpireTime(time))
    
    if time < zmmd.EXPIRE_WARNING * 86400 then
        mmu.SetLabelColor(expire, zmmd.LAYOUTS[current].timeWarning)
    else
        mmu.SetLabelColor(expire, zmmd.LAYOUTS[current].timeColor)
    end
end


-- creates tooltip for money icon
function zmm.MouseOverCoins()
    local row  = SystemData.MouseOverWindow.name
    local id   = WindowGetId(row)
    local mail = {}
    
    if MailWindow.SelectedTab == MailWindow.TABS_INBOX then
        mail = mmi.GetMailDataByNumber(id)
    elseif MailWindow.SelectedTab == MailWindow.TABS_AUCTION then
        mail = mma.GetMailDataByNumber(id)
    else
        return
    end
    
    if mail.attachmentIconIDs ~= nil and #mail.attachmentIconIDs > 1 and MailWindowUtils.ContainsUntakenAttachmentItems(mail) then
        MailWindow.OnMouseOverAttachmentItem()
    elseif mail.attachmentMoney > 0 and not mail.isCOD and not mail.isMoneyTaken then
        local anchor = {Point = "top", RelativeTo = row, RelativePoint = "bottom", XOffset = 0, YOffset = -10}
        Tooltips.CreateMoneyTooltip(zL["TOOLTIP_MONEY"], mail.attachmentMoney, row, anchor)
    end
end


function zmm.UpdateLayout(type)
    local current = zmmd.LAYOUT_INBOX
    local parent  = "MailWindowTab" .. type
    local space   = parent .. "SpaceAboveList"
    local btnBg   = parent .. "BarBackground"
    local btnOpen = parent .. "OpenMessageButton"
    local btnDel  = parent .. "DeleteMessageButton"
    local btnRet  = parent .. "ReturnMessageButton"
    local bg      = "zMailMod" .. type .. "Background"
    local btnMon  = "zMailMod" .. type .. "SelectMoneyButton"
    local btnSAll = "zMailMod" .. type .. "SelectAllButton"
    local btnOSel = "zMailMod" .. type .. "OpenSelectedButton"

    zmm.SetRowsNumber(type)
    zmm.SetListRowTints()

    if type == "Inbox" then
        if zmmd.LAYOUT_INBOX ~= 0 then
            WindowClearAnchors(space)
            WindowAddAnchor   (space, "topleft", parent, "topleft", 0, 0)
            WindowAddAnchor   (space, "topright", parent, "bottomright", 0, 50)
        else
            WindowClearAnchors(space)
            WindowAddAnchor   (space, "topleft", parent, "topleft", 0, 0)
            WindowAddAnchor   (space, "topright", parent, "bottomright", 0, 65)
        end
    end

    -- put separator bar above the background
    WindowSetLayer(parent .. "DisplaySeperator", Window.Layers.DEFAULT)

    -- create background
    if not DoesWindowExist(bg) then
        CreateWindowFromTemplate(bg, "zMailModBackgroundTemplate", parent)
        WindowAddAnchor         (bg, "bottomleft", parent .. "SpaceAboveList", "topleft", 7, 0)
        WindowAddAnchor         (bg, "topright", parent .. "DisplaySeperator", "topright", -7, 0)
        WindowSetTintColor      (bg, 217, 191, 157)
    end
    WindowSetShowing(bg, zmmd.LAYOUT_INBOX == 1)

    -- using inbox only, auction list has the same number of rows
    for i = 1, MailWindowTabInboxList.numVisibleRows, 1 do
        local row      = parent .. "ListRow" .. i
        local rowBg    = row .. "RowBackground"
        local header   = row .. "Header"
        local item     = row .. "AttachmentButton"
        local icon     = row .. "Icon"
        local from     = header .. "FromText"
        local subject  = header .. "SubjectText"
        local expire   = header .. "ExpiresText"
        local cod      = "zMailMod" .. type .. "COD" .. i
        local checkbox = "zMailMod" .. type .. "Checkbox" .. i
        local sep      = "zMailMod" .. type .. "Separator" .. i

        -- create checkbox
        if not DoesWindowExist(checkbox) then
            CreateWindowFromTemplate(checkbox, "zMailModCheckboxTemplate", row)
            WindowSetId             (checkbox, i)
        end

        -- create COD label
        if not DoesWindowExist(cod) then
            CreateWindowFromTemplate(cod, "zMailModCODLabel", row)
            WindowAddAnchor         (cod, "bottomright", item, "bottomright", 0, 0)
            LabelSetText            (cod, GetMailString(StringTables.Mail.LABEL_MAIL_CHECKBOX_COD))
        end

        -- create message separator
        if not DoesWindowExist(sep)then
            CreateWindowFromTemplate(sep, "zMailModMailSeparator", row)
            WindowSetDimensions     (sep, 465, 2)
            WindowAddAnchor         (sep, "bottom", row, "top", 0, 0)
        end
        WindowSetShowing(sep, zmmd.LAYOUT_INBOX == 1)
        
        -- clear anchors
        WindowClearAnchors(rowBg)
        WindowClearAnchors(item)
        WindowClearAnchors(from)
        WindowClearAnchors(subject)
        WindowClearAnchors(expire)

        -- set layers for icons
        WindowSetLayer(item, Window.Layers.SECONDARY)
        WindowSetLayer(icon, Window.Layers.SECONDARY)

        -- set fonts for labels
        mmu.SetLabelFont(from,    zmmd.LAYOUTS[current].font)
        mmu.SetLabelFont(subject, zmmd.LAYOUTS[current].font)
        mmu.SetLabelFont(expire,  zmmd.LAYOUTS[current].font)

        -- misc
        WindowRegisterCoreEventHandler(icon, "OnMouseOver", "zmm.MouseOverCoins")
        LabelSetTextAlign(expire, zmmd.LAYOUTS[current].timeAlign)
        WindowSetDimensions(icon, 50, 50)
        
        -- format expire time
        WindowClearAnchors (header .. "Expires")
        WindowAddAnchor    (header .. "Expires", "left", expire, "right", -5, 0)
        WindowSetDimensions(expire, 110, 26)

        if zmmd.LAYOUT_INBOX ~= 0 then
            WindowAddAnchor    (rowBg, "topleft", row, "topleft", 4, 2)
            WindowAddAnchor    (rowBg, "bottomright", row, "bottomright", -4, -2)
            WindowAddAnchor    (item, "right", checkbox, "left", 15, 0)
            WindowSetScale     (item, 1.33 * InterfaceCore.GetScale())

            WindowAddAnchor    (from, "topright", item, "topleft", 15, -2)
            WindowAddAnchor    (subject, "bottomright", item, "bottomleft", 15, -1)
            WindowAddAnchor    (expire, "topright", rowBg, "topright", -10, 5)
        else
            WindowAddAnchor    (rowBg, "topleft", row, "topleft", 4, 4)
            WindowAddAnchor    (rowBg, "bottomright", row, "bottomright", -4, -4)
            WindowAddAnchor    (item, "topright", row, "topright", -10, 5)
            WindowSetScale     (item, 0.85 * InterfaceCore.GetScale())
            WindowAddAnchor    (from, "right", header .. "From", "left", 10, 0)
            WindowAddAnchor    (subject, "right", header .. "Subject", "left", 10, 0)
            WindowAddAnchor    (expire, "bottomright", rowBg, "bottomright", -10, 0)
        end
        
        -- set fonts for other labels
        mmu.SetLabelFont(header .. "From"    , zmmd.LAYOUTS[current].font)
        mmu.SetLabelFont(header .. "Subject" , zmmd.LAYOUTS[current].font)
        mmu.SetLabelFont(header .. "Sent"    , zmmd.LAYOUTS[current].font)
        mmu.SetLabelFont(header .. "SentText", zmmd.LAYOUTS[current].font)
        mmu.SetLabelFont(header .. "Expires" , zmmd.LAYOUTS[current].font)
        
        -- show/hide other labels
        WindowSetShowing(header .. "From"    , current == 0)
        WindowSetShowing(header .. "Subject" , current == 0)
        WindowSetShowing(header .. "Sent"    , current == 0)
        WindowSetShowing(header .. "SentText", current == 0)
        WindowSetShowing(header .. "Expires" , current == 0)
    end
    
    -- reanchor default buttons
    WindowClearAnchors (btnOpen)
    WindowAddAnchor    (btnOpen, "topleft", btnBg, "topleft", 20, 17)
    WindowSetDimensions(btnOpen, 155, 39)

    WindowClearAnchors (btnDel)
    WindowAddAnchor    (btnDel, "top", btnBg, "top", 0, 17)
    WindowSetDimensions(btnDel, 155, 39)

    WindowClearAnchors (btnRet)
    WindowAddAnchor    (btnRet, "topright", btnBg, "topRight", -20, 17)
    WindowSetDimensions(btnRet, 155, 39)

    -- create zMailMod buttons
    if not DoesWindowExist(btnSAll) then
        CreateWindow   (btnSAll, true)
        WindowSetParent(btnSAll, parent)
        ButtonSetText  (btnSAll, zL["BUTTON_SELECT_ALL"])
    end
    
    if not DoesWindowExist(btnOSel) then
        CreateWindow   (btnOSel, true)
        WindowSetParent(btnOSel, parent)
        ButtonSetText  (btnOSel, zL["BUTTON_OPEN_SELECTED"])
    end
    
    if not DoesWindowExist(btnMon) and type == "Auction" then
        CreateWindow   (btnMon, true)
        WindowSetParent(btnMon, parent)
        ButtonSetText  (btnMon, zL["BUTTON_SELECT_MONEY"])
    end
end


function zmm.UpdateListRowIcon(row, data)
    local item     = row .. "AttachmentButton"
    local icon     = row .. "Icon"
    local current  = zmmd.LAYOUT_INBOX
    
    if current == 0 then
        WindowClearAnchors(icon)
        WindowAddAnchor   (icon, "left", row, "left", 40, 0)
        DynamicImageSetTextureDimensions(icon, 65, 62)
        WindowSetShowing  (icon, true)
    else
        -- show item(s)
        if data.attachmentIconIDs ~= nil and #data.attachmentIconIDs > 0 and MailWindowUtils.ContainsUntakenAttachmentItems(data) then
            -- 1 item
            if #data.attachmentIconIDs == 1 then
                WindowSetShowing           (icon, false)
                WindowSetShowing           (item, true)
            -- more items
            else
                WindowSetDimensions        (icon, 50, 50)
                DynamicImageSetTextureSlice(icon, "package-unread")
                DynamicImageSetTextureDimensions(icon, 65, 62)
                WindowClearAnchors         (icon)
                WindowAddAnchor            (icon, "topleft", item, "topleft", 0, 0)
                WindowSetShowing           (icon, true)
                WindowSetShowing           (item, false)
            end
        -- show coins
        elseif data.attachmentMoney > 0 and not data.isCOD and not data.isMoneyTaken then
            WindowSetDimensions        (icon, 32, 50)
            DynamicImageSetTextureSlice(icon, "coins")
            DynamicImageSetTextureDimensions(icon, 20, 32)
            WindowClearAnchors         (icon)
            WindowAddAnchor            (icon, "topleft", item, "topleft", 8, 4)
            WindowSetShowing           (icon, true)
            WindowSetShowing           (item, false)
            WindowRegisterCoreEventHandler(icon, "OnMouseOver", "mmi.MouseOverCoins")
        -- show letter
        else
            WindowSetDimensions        (icon, 50, 50)
            DynamicImageSetTextureSlice(icon, "mail-read")
            DynamicImageSetTextureDimensions(icon, 65, 62)
            WindowClearAnchors         (icon)
            WindowAddAnchor            (icon, "topleft", item, "topleft", 0, 0)
            WindowSetShowing           (icon, true)
            WindowSetShowing           (item, false)
        end
    end

    if data.hasBeenRead then
        mmu.SetIconTint(item, zmmd.LAYOUTS[current].itemRead)
        mmu.SetIconTint(icon, zmmd.LAYOUTS[current].itemRead)
    else
        mmu.SetIconTint(item, zmmd.LAYOUTS[current].itemUnread)
        mmu.SetIconTint(icon, zmmd.LAYOUTS[current].itemUnread)
    end
end


function zmm.UpdateListRowLabels(header, hasBeenRead)
    local current = zmmd.LAYOUT_INBOX

    if hasBeenRead then
        mmu.SetLabelColor(header .. "FromText"   , zmmd.LAYOUTS[current].fromRead)
        mmu.SetLabelColor(header .. "SubjectText", zmmd.LAYOUTS[current].subjRead)
    else
        mmu.SetLabelColor(header .. "FromText"   , zmmd.LAYOUTS[current].fromUnread)
        mmu.SetLabelColor(header .. "SubjectText", zmmd.LAYOUTS[current].subjUnread)
    end
end


function zmm.MailOpened(data, type)
    mmv.UpdateBottomBar()

    if zmmd.JOB == zmmd.OPENING then 
        MailWindowTabMessage.OnClose()
        
        if zmmd.STATE == zmmd.INBOX then
            mmi.MailOpened(data, type)
        elseif zmmd.STATE == zmmd.AUCTION then
            mma.MailOpened(data, type)
        end
    end
end


-- checks if mail can be safely deleted
function zmm.CanBeDeleted(data)
    if data.isCOD then return false end

    if (data.attachmentMoney > 0 and not data.isMoneyTaken) or MailWindowUtils.ContainsUntakenAttachmentItems(data) or not data.hasBeenRead then
        return false
    else
        return true
    end
end


-- handles changes in user's inventory (money or items)
function zmm.InventoryUpdated()
    mmu.debug("[zmm.InventoryUpdated]")
    mmu.debug("   > if opening")
    if zmm.IsActive("MailWindow") and zmmd.JOB == zmmd.OPENING then
    mmu.debug("   > yes, delete")
        if zmmd.STATE == zmmd.INBOX then
            mmi.DeleteMail(mmi.current, GameData.MailboxType.PLAYER)
        elseif zmmd.STATE == zmmd.AUCTION then
            mma.DeleteMail(mma.current, GameData.MailboxType.AUCTION)
        end
    end
end


-- updates numbers of messages in inbox and auction
function zmm.MailUpdated()
    mmi.MailUpdated()
    mma.MailUpdated()
end


-- changes mail window's dimensions to add/remove background for zMailMod buttons
function zmm.UpdateBottomBar()
    if MailWindow.SelectedTab == MailWindow.TABS_INBOX or MailWindow.SelectedTab == MailWindow.TABS_AUCTION then
        WindowSetDimensions("MailWindow", 525, 755)
        WindowSetDimensions(MailWindow.Tabs[MailWindow.SelectedTab].window .. "BarBackground", 525, 110)
    else
        WindowSetDimensions("MailWindow", 525, 705)
        WindowSetDimensions(MailWindow.Tabs[MailWindow.SelectedTab].window .. "BarBackground", 525, 60)
    end
end


-- updates recipients list in Rolodex
function zmm.RolodexCheckInput()
    if DoesWindowExist(zmmd.ROLODEX) then
        Rolodex.CheckInput()
    end
end


-- enables filtering names in Rolodex addon
function zmm.RegisterRolodex()
    if DoesWindowExist(zmmd.ROLODEX) then
        Rolodex.RegisterEditBox(zmmd.WINDOW_MASSMAIL, zMailModMassMailToEditBox, "zMailModMassMailToEditBox")
    end
end
