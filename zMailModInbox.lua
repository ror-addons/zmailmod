--[[
    --> file: zMailModInbox.lua
    --> file: zMailModInbox.xml

    --> description: managing Inbox window

    --> author: zoog
    --> email: ZooGTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

zMailModInbox = {}

mmi = zMailModInbox

--------------------[VARIABLES]-------------------------------------------------

local win = zmmd.WINDOW_INBOX

mmi.checkbox = {}
mmi.current  = {}
mmi.attach   = {}
mmi.lastID   = 0

--------------------[INITIALIZATION]--------------------------------------------

-- initialization
function mmi.Initialize()
    zmm.UpdateLayout("Inbox")
    mmi.ResetAttachmentsInfo()
end


-- resets information about currently processed attachments
function mmi.ResetAttachmentsInfo()
    mmi.attach =
    {
        money = 0,
        items  = {}
    }
end


-- initialization of checkboxes
function mmi.ClearCheckboxes()
    mmi.checkbox = {}
    mmi.Populate()
end


-- checking and unchecking checkboxes
function mmi.SelectMail()
    local name  = SystemData.ActiveWindow.name
    local winID = WindowGetId(name)
    local id    = ListBoxGetDataIndex(win .. "List", winID)

    if mmi.checkbox[id] ~= 0 and mmi.checkbox[id] ~= nil then
        mmi.checkbox[id] = nil
        ButtonSetPressedFlag("zMailModInboxCheckbox" .. winID, false)
    else
        mmi.checkbox[id] = MailWindowTabInbox.listData[id].messageID
        ButtonSetPressedFlag("zMailModInboxCheckbox" .. winID, true)
    end
end


-- populating function
function mmi.Populate()
    zmm.hooked.MailWindowInbox_Populate()

    if MailWindowTabInboxList.PopulatorIndices ~= nil then
        for row, id in ipairs(MailWindowTabInboxList.PopulatorIndices) do
            local label = "zMailModInboxCOD" .. row
            local isCOD = MailWindowTabInbox.listData[id].isCOD and not MailWindowTabInbox.listData[id].isCODPaid and zmmd.LAYOUT_INBOX ~= 0
            if DoesWindowExist(label) then WindowSetShowing(label, isCOD) end
            zmm.UpdateListRow("Inbox", row, id)
        end
    end
end


-- populates row icon
function mmi.PopulateItemIcon(row, data)
    zmm.hooked.MailWindowInbox_PopulateItemIcon(row, data)
    if zmmd.LAYOUT_INBOX ~= 0 then WindowSetShowing(row .. "MultipleAttachmentsImage", false) end
    zmm.UpdateListRowIcon(row, data)
end


-- displays number of messages
function mmi.HeadersNumber()
    zmm.hooked.MailWindowInbox_HeadersNumber()
    if #MailWindowTabInbox.listData ~= 1 then
        LabelSetText(win .. "MessageNumberText", #MailWindowTabInbox.listData .. L" " .. zL["MESSAGES"])
    else
        LabelSetText(win .. "MessageNumberText", #MailWindowTabInbox.listData .. L" " .. zL["MESSAGE"])
    end
end


-- selects all messages
function mmi.SelectAll()
    local selected = mmi.AllSelected()

    for i = 1, #MailWindowTabInbox.listDataOrder, 1 do
        if selected then
            mmi.checkbox = {}
            break
        else
            mmi.checkbox[i] = MailWindowTabInbox.listData[i].messageID
        end
    end

    mmi.Populate()
end


-- checks if all messages have been selected
function mmi.AllSelected()
    for i = 1, #MailWindowTabInbox.listDataOrder, 1 do
        if mmi.checkbox[i] == 0 or mmi.checkbox[i] == nil then return false end
    end

    return true
end


-- Updates selected checkbox list
function mmi.MailUpdated()
    mmi.HeadersNumber()

    local mails = mmi.GetMailsIdx()
    local cache = DataUtils.CopyTable(mmi.checkbox)
    mmi.checkbox = {}
    
    for i, id in pairs(cache) do
        if mails[id] ~= nil then
            mmi.checkbox[mails[id]] = id
        end
    end
    
    mmi.Populate()
end


-- corrects sentTimeStamp values of each mail to prevent opened mail going down on the list
function mmi.HeaderUpdated(data, type)
    local id    = data[1].messageID
    local mails = mmi.GetMailsIdx()

    if mails[id] ~= nil then
         local prev = mmi.GetMailDataById(id).sentTimeStamp
         local curr = data[1].messageSendDelta * (-1)
         local time = curr - prev
         for i, mail in pairs(MailWindowTabInbox.listData) do
             MailWindowTabInbox.listData[i].sentTimeStamp = MailWindowTabInbox.listData[i].sentTimeStamp + time
         end
    end

    zmm.hooked.MailWindowInbox_HeaderUpdated(data, type)

    if id == mmi.current.messageID then
        mmi.current = mmi.GetMailDataById(id)
    end
end


-- hides original coins icon
function mmi.PopulateCoins(row, data)
    zmm.hooked.MailWindowInbox_PopulateCoins(row, data)
    if zmmd.LAYOUT_INBOX ~= 0 then WindowSetShowing(row .. "CoinsImage", false) end
end


-- creates tooltip for cod label
function mmi.OnMouseOverCOD()
    local name    = SystemData.MouseOverWindow.name
    local parent  = WindowGetParent(name)
    local idp     = WindowGetId(parent)
    local id      = ListBoxGetDataIndex(win .. "List", idp)
    local data    = MailWindowTabInbox.listData[id]
    local g, s, b = MoneyFrame.ConvertBrassToCurrency(data.money)
    local text    = GetFormatStringFromTable("mailstrings", StringTables.Mail.TOOLTIP_MAIL_HEADER_COINS_COD, {g, s, b})

    Tooltips.CreateTextOnlyTooltip(name, nil)
    Tooltips.SetTooltipText(1, 1, text)
    Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING)
    Tooltips.Finalize()

    local anchor = {Point="top", RelativeTo=parent .. "AttachmentButton", RelativePoint="bottom", XOffset=0, YOffset=-10}
    Tooltips.AnchorTooltip(anchor)
    Tooltips.SetTooltipAlpha(1)
end


-- returns table with mails' list idx and messages idx
function mmi.GetMailsIdx()
    local mails = {}

    for i = 1, #MailWindowTabInbox.listDataOrder, 1 do
        mails[MailWindowTabInbox.listData[i].messageID] = i
    end

    return mails
end


-- returns data of the mail with given id (messageID)
function mmi.GetMailDataById(id)
    local mails = mmi.GetMailsIdx()
    return MailWindowTabInbox.listData[mails[id]]
end


-- returns data of the mail with given number (row number 1-7 or 1-5)
function mmi.GetMailDataByNumber(num)
    local id = ListBoxGetDataIndex(win .. "List", num)
    return MailWindowTabInbox.listData[id]
end


-- sorts messages
function mmi.SortLetters()
    local mails = {}

    for i, id in pairs(mmi.checkbox) do
        table.insert(mails, i)
    end

    table.sort(mails)

    return mails
end


-- returns first non-COD message
function mmi.GetNextLetter()
    local mails = mmi.SortLetters()

    for i, id in pairs(mails) do
        local mail = MailWindowTabInbox.listData[id]
        if not mail.isCOD then return mail end
    end

    return false
end

--------------------[OPENING]---------------------------------------------------

-- starts opening job
function mmi.OpenSelected()
    mmu.debug("[mmi.OpenSelected]")
    if zmmd.JOB == zmmd.IDLE then
        mmt.errors = 0
        zmmd.JOB = zmmd.OPENING
        zmmd.STATE = zmmd.INBOX
        mmi.Open()
    end
end


-- stops opening
function mmi.InboxStop()
    MailWindowTabMessage.OnClose()
    zmm.UpdateResults(89)
    mmt.errors = 0
    mmi.current = {}
    zmmd.JOB = zmmd.IDLE
    zmmd.STATE = zmmd.READY
    mmi.ClearCheckboxes()
    return false
end


-- opens mail
function mmi.Open()
    mmu.debug("[mmi.Open]")
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.INBOX then
        local mail = mmi.GetNextLetter()
        if not mail then mmu.debug("   > end") mmi.InboxStop() return end
        mmi.current = mail
        mmu.debug("   > open")
        SendMailboxCommand(MailWindow.MAILBOX_OPEN_MESSAGE, GameData.MailboxType.PLAYER, mail.messageID, L"", L"", L"", 0, {}, {}, false)
        mmu.debug("   > ...")
    end
end


-- takes attachments from mail
function mmi.MailOpened(data, type)
    mmu.debug("[mmi.MailOpened]")
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.INBOX then
        mmu.debug("   > close msg")
        MailWindowTabMessage.OnClose()
        mmu.debug("   > if curent")
        if data.messageID == mmi.current.messageID then
        mmu.debug("   > if can be deleted")
            if zmm.CanBeDeleted(mmi.current) then
                mmu.debug("   > yes, delete")
                mmi.DeleteMail(mmi.current, type)
            else
                mmu.debug("   > no, save info")
                mmi.SaveAttachmentsInfo(mmi.current, data)
                mmu.debug("   > no, take all")
                SendMailboxCommand(MailWindow.MAILBOX_TAKE_ALL, GameData.MailboxType.PLAYER, data.messageID, L"", L"", L"", 0, {}, {}, false)
                mmu.debug("   > ...")
            end
        end
    end
end


-- saves information about attachmens in current message
function mmi.SaveAttachmentsInfo(mail, data)
    if mail.isCOD then return false end
    if mail.attachmentMoney == 0 and #mail.attachmentIconIDs == 0 then return false end
    if mail.attachmentMoney > 0 and not mail.isMoneyTaken then mmi.attach.money = mail.attachmentMoney end
    if #mail.attachmentIconIDs > 0 and MailWindowUtils.ContainsUntakenAttachmentItems(mail) then
        for i = 1, #mail.attachmentIconIDs, 1 do
            if mail.attachmentsTakenTable[i] == false then
                table.insert(mmi.attach.items, {icon = data.itemsAttached[i].iconNum, count = data.itemsAttached[i].stackCount, rarity = data.itemsAttached[i].rarity, name = data.itemsAttached[i].name})
            end
        end
    end
end


-- deletes mail
function mmi.DeleteMail(data, type)
    mmu.debug("[mmi.DeleteMail")
    mmu.debug("   > can be deleted?")
    if zmm.CanBeDeleted(data) then
        mmu.debug("   > yes")
        local logData =
        {
            sender  = data.from,
            subject = data.subject,
            type    = GetMailString(MailWindow.Tabs[MailWindow.TABS_INBOX].label),
            money   = 0,
            item    = L"0#0#0#",
        }

        if mmi.lastID ~= data.messageID then
            mmi.lastID = data.messageID
            if #mmi.attach.items > 0 then
                for i = 1, #mmi.attach.items, 1 do
                    logData.money = mmi.attach.money
                    logData.item = L"" .. mmi.attach.items[i].icon .. L"#" .. mmi.attach.items[i].count .. L"#" .. mmi.attach.items[i].rarity .. L"#" .. mmi.attach.items[i].name
                    mml.AddEntry(logData)
                    mmi.attach.money = 0
                end
            else
                logData.money = mmi.attach.money
                logData.item = L"0#0#0#"
                mml.AddEntry(logData)
            end
        end
        mmi.ResetAttachmentsInfo()
        mmu.debug("   > deleting")
        SendMailboxCommand(MailWindow.MAILBOX_DELETE_MESSAGE, GameData.MailboxType.PLAYER, data.messageID, L"", L"", L"", 0, {}, {}, false)
        mmu.debug("   > ...")
    else
        mmu.debug("   > no, open")
        mmi.Open()
    end
end


-- continues work after mail has been deleted
function mmi.MailDeleted(id, type)
    mmu.debug("[mmi.MailDeleted]")
    mmu.debug("   > if opening")
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.INBOX then
        mmi.current = {}
        mmi.MailUpdated()
        mmu.debug("   > open next")
        mmi.Open()
    end
end


-- event handler for changes in user's inventory
function mmi.ItemsUpdated()
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.INBOX then
        mmi.DeleteMail(mmi.current, GameData.MailboxType.PLAYER)
    end
end


-- event handler for changes in user's money
function mmi.MoneyUpdated()
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.INBOX then
        mmi.DeleteMail(mmi.current, GameData.MailboxType.PLAYER)
    end
end